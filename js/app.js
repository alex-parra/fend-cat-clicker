jQuery(document).ready(function($) {
  app.init();
});


const app = {
  data: [
    {name: 'Cat in Jeans', img: 'imgs/cat-in-jeans.jpg', clicks: 0, el: null},
    {name: 'Cat looking over table', img: 'imgs/cat-looking-over-table.jpg', clicks: 0, el: null},
    {name: 'Cat licks', img: 'imgs/cat-licks.jpg', clicks: 0, el: null},
    {name: 'Cat Santa', img: 'imgs/cat-santa.jpg', clicks: 0, el: null},
    {name: 'Cat Sleeping', img: 'imgs/cat-sleeping.jpg', clicks: 0, el: null},
  ],

  init: function(){
    this.view.init(this.data, this.afterUiInit.bind(this));
  },
  afterUiInit: function(){
    this.showItem(_.head(this.data).name);
  },
  showItem: function(itemName){
    var item = _.find(this.data, {name: itemName});
    this.view.showItem(item);
  },
  itemClicked: function(itemName){
    var item = _.find(this.data, {name: itemName});
    item.clicks++;
    this.showItem(itemName);
  }
}


app.view = {
  sideList: null,
  mainView: null,
  init: function(data, callback){
    this.sideList = $('<div class="sideList"></div>');
    this.mainView = $('<div class="mainView"></div>');

    let listItems = [];

    data.forEach(function(i){
      let $item = $('<div class="item" data-name="'+ i.name +'">'+ i.name +'</div>');
      listItems.push($item);
    });
    this.sideList.append(listItems);

    this.sideList.on('click', '.item', this.handleListItemClick);
    this.mainView.on('click', '.item', this.handleMainViewClick);

    $('body').prepend(this.sideList, this.mainView);

    callback();
  },
  handleListItemClick: function(ev){
    app.showItem($(this).data('name'));
  },
  handleMainViewClick: function(ev){
    app.itemClicked($(this).data('name'));
  },
  showItem: function(item){
    var $item = $('<div class="item" data-name="'+ item.name +'"></div>');
    $item.css('background-image', 'url('+ item.img +')');
    $item.append('<div class="name" data-clicks="'+ item.clicks +'">'+ item.name +'</div>');
    this.mainView.html($item);
  }
}
